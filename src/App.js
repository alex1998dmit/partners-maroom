import logo from './logo.svg';
import './App.css';
import Header from "./components/common/Header/Header";
import SeedRound from "./components/common/SeedRound/SeedRound";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";
import Contacts from "./components/common/Contacts";

const appTheme = createMuiTheme({
  typography: {
    fontFamily: [
      'Montserrat',
      'Roboto'
    ].join(','),
    h1: {
      fontSize: '48px',
      fontWeight: '600'
    },
    h3: {
      fontSize: '18px'
    },
    subtitle1: {
      fontSize: '18px'
    },
    subtitle2: {
      fontSize: '14px'
    }
  }
})

function App() {
  return (
    <MuiThemeProvider theme={appTheme}>
      <div className="App">
        <Header />
        <SeedRound />
        <Contacts />
      </div>
    </MuiThemeProvider>
  )
}

export default App;
