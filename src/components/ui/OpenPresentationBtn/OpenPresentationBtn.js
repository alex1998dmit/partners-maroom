import React from "react"
import {Button, makeStyles} from "@material-ui/core";
import MaroomIcon from "../../icons/Maroom";
import OpenIcon from "../../icons/OpenModal";

const useStyles = makeStyles({
  root: {
    backgroundColor: '#F57C00',
    border: 'none',
    borderRadius: '66px',
    padding: '11px 16px',
    textTransform: 'none',
    fontWeight: 500,
    fontSize: '14px',
    lineHeight: '18px',
    color: 'white',
    '& svg': {
      '& path': {
        fill: 'white'
      }
    },
    '&:hover': {
      backgroundColor: '#EA7600'
    }
  },
  iconWrapper: {
    // paddingLeft: '45px'
  }
})

const OpenPresentationBtn = (props) => {
  const classes = useStyles()
  return (
    <Button
      classes={{
        root: classes.root,
        endIcon: classes.iconWrapper
      }}
      variant={'outlined'}
      endIcon={<OpenIcon />}
    >
      Open presentation
    </Button>
  )
}

export default OpenPresentationBtn
