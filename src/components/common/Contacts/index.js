import React from "react"
import {Container, Grid, Icon, IconButton, Link, makeStyles, Typography} from "@material-ui/core";
import MailIcon from "../../icons/Mail";
import LinkedInColorIcon from "../../icons/LinkedInColor";
import TwitterColorIcon from "../../icons/TwitterColorIcon";
import FacebookColorIcon from "../../icons/FacebookColor";

const useStyles = makeStyles(theme => ({
  wrapper: {
    height: '32vh',
    display: 'flex',
    alignItems: 'center',
    borderTop: '1px solid #EAECEF',
    [theme.breakpoints.down('md')]: {
      // height: 'unset'
    }
  },
  title: {
    lineHeight: '150%',
    paddingBottom: '32px',
    [theme.breakpoints.down('md')]: {
      fontSize: '24px',
      paddingBottom: '16px'
    }
  },
  linksContainer: {
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'start'
    }
  },
  emailBtn: {
    display: 'flex',
    alignItems: 'center',
    paddingRight: '32px',
    // borderRight: '1px solid #EAECEF'
    borderRight: '1px solid #EAECEF',
    width: 'fit-content',
    cursor: 'pointer',
    [theme.breakpoints.down('md')]: {
      border: 'none',
      paddingRight: 0
    }
  },
  emailValue: {
    fontSize: '14px',
    fontWeight: 500,
    color: 'black !important',
  },
  emailKey: {
    fontSize: '14px',
    fontWeight: 500,
    color: '#7F8285 !important'
  },
  emailTitles: {
    paddingLeft: '18px'
  },
  socialBtns: {
    paddingLeft: '32px',
    [theme.breakpoints.down('md')]: {
      paddingLeft: 0,
      marginTop: '4px'
    }
  },
  btnRoot: {
    backgroundColor: 'white',
    marginRight: '16px',
    '&:hover': {
      boxShadow: '0px 5px 16px rgba(0, 0, 0, 0.1)',
      background: 'white'
    },
    [theme.breakpoints.down('md')]: {
      marginRight: '4px'
    }
  }
}))

const SocialBtn = (props) => {
  const classes = useStyles()
  return (
    <IconButton className={classes.btnRoot}>
      {props.children}
    </IconButton>
  )
}

const Contacts = () => {
  const classes = useStyles()
  return (
    <div className={classes.wrapper}>
      <Container>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant={'h1'} align={'left'} className={classes.title}>
              Are you interested? <br />
              Contact us
            </Typography>
          </Grid>
          <Grid item xs={12} lg={5} container className={classes.linksContainer}>
            <div className={classes.emailBtn}>
              <Icon>
                <MailIcon />
              </Icon>
              <Link className={classes.emailTitles} underline={'none'}>
                <Typography align={'left'} className={classes.emailValue}>
                  kuzin@maroom.ru
                </Typography>
                <Typography align={'left'} className={classes.emailKey}>
                  почта
                </Typography>
              </Link>
            </div>
            <div className={classes.socialBtns}>
              <SocialBtn>
                <LinkedInColorIcon />
              </SocialBtn>
              <SocialBtn>
                <TwitterColorIcon />
              </SocialBtn>
              <SocialBtn>
                <FacebookColorIcon />
              </SocialBtn>
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default Contacts
