import React from "react"
import {Grid, Icon, makeStyles, Typography} from "@material-ui/core";
import LinkedInIcon from "../../../icons/LinkedIn";
import FacebookIcon from "../../../icons/Facebook";
import TwitterIcon from "../../../icons/Twitter";
import useWindowWidth from "../../../../hooks/useWindowWidth";

const useStyles = makeStyles({
  pointRoot: {
    display: 'flex',
    alignItems: 'center',
    padding: '11px 14px',
    position: 'relative',
    backgroundColor: 'white',
    borderRadius: '12px'
  },
  pointDot: {
    width: '12px',
    height: '12px',
    borderRadius: '66px',
  },
  pointInfo: {
    paddingLeft: '16px'
  },
  pointTitle: {
    fontWeight: '600',
    lineHeight: '24px'
  },
  pointMoney: {
    color: '#7F8285'
  },
  links: {
    position: 'absolute',
    top: '20px',
    right: '20px'
  }
})

const Point = (props) => {
  const { item } = props
  const classes = useStyles()
  return (
    <div className={classes.pointRoot}>
      <div className={classes.pointDot} style={{ backgroundColor: item.color }} />
      <div className={classes.pointInfo}>
        <Typography variant={'h3'} align={'left'} className={classes.pointTitle}>
          {item.title}
        </Typography>
        <Typography variant={'subtitle2'} className={classes.pointMoney} align={'left'}>
          ${item.money.toLocaleString()} • {item.percent}%
        </Typography>
        <div className={classes.links}>
          <Icon>
            <LinkedInIcon />
          </Icon>
          <Icon>
            <FacebookIcon />
          </Icon>
          <Icon>
            <TwitterIcon />
          </Icon>
        </div>
      </div>
    </div>
  )
}

const Points = (props) => {
  const width = useWindowWidth()
  return (
    <Grid container spacing={width > 1280 ? 2 : 1}>
      {props.items.map((item, index) => (
        <Grid item xs={12} sm={12} md={12} lg={6} key={index}>
          <Point item={item} />
        </Grid>
      ))}
    </Grid>
  )
}

export default Points
