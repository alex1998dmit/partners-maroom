import React from "react"
import EChartsReact from "echarts-for-react";
import useWindowWidth from "../../../../hooks/useWindowWidth";

const Diagram = (props) => {
  const data = props.data
  const width = useWindowWidth()
  const isDesktop = width > 1279
  const options = {
    tooltip: 'item',
    legend: {
      show: false
    },
    series: [
      {
        name: 'seed round',
        type: 'pie',
        radius: [isDesktop ? 120 : 90, isDesktop ? 181 : 131],
        center: [isDesktop ? '50%' : '50%', isDesktop ? '70%' : '50%'],
        height: 364,
        itemStyle: {
          borderRadius: 6,
          borderColor: '#fff',
          borderWidth: 4
        },
        label: {
          show: true,
          position: 'center',
          fontSize: isDesktop ? 40 : 32,
          fontFamily: 'Montserrat',
          fontWeight: 500,
          formatter: () => '$' + data.reduce((acc, item) => acc + item.value, 0).toLocaleString(),
          color: 'black'
        },
        // emphasis: {
        //   label: {
        //     show: true,
        //     fontSize: '40',
        //     fontWeight: 'bold'
        //   }
        // },
        // labelLine: {
        //   show: true,
        // },
        data
      }
    ]
  }

  return (
    <EChartsReact
      option={options}
      opts={{ renderer: 'svg' }}
      style={{
        height: isDesktop ? '500px' : '350px'
      }}
    />
  )
}

export default Diagram
