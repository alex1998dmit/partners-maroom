import React from "react"
import {Container, Grid, makeStyles, Typography} from "@material-ui/core"
import Points from "./Points";
import Diagram from "./Diagram";
import OpenPresentationBtn from "../../ui/OpenPresentationBtn/OpenPresentationBtn";
import Contacts from "../Contacts";
import useWindowWidth from "../../../hooks/useWindowWidth";

const useStyles = makeStyles(theme => ({
  investsContainer: {
    height: 'calc(63vh - 80px)',
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      height: 'unset',
      paddingTop: '50px',
      paddingBottom: '50px'
    }
  },
  title: {
    fontWeight: "600",
    paddingBottom: '12px',
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      fontSize: '32px',
      paddingBottom: '4px',
    }
  },
  subtitle: {
    color: '#7F8285',
    paddingBottom: '32px',
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      fontSize: '14px',
      paddingBottom: '16px'
    }
  },
  presentationBtn: {
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
      // paddingBottom: '22px'
    }
  },
  wrapper: {
   backgroundColor: '#F2F2F2',
  },
  backgroundEllipse: {
    backgroundImage: `url(/ellipse.svg)`,
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundRepeat: 'no-repeat',
    width: '627px',
    height: '627px',
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  }
}))

const items = [
  {
    title: 'IVP',
    percent: 40,
    color: '#F57C00',
    money: 400000
  },
  {
    title: 'Sergey Pirogov',
    percent: 9,
    color: '#27AE60',
    money: 30000
  },
  {
    title: 'Fidelty',
    percent: 15,
    color: '#EB5757',
    money: 60000
  },
  {
    title: 'Wellington',
    percent: 15,
    color: '#9B51E0',
    money: 60000
  },
  {
    title: 'Dan Abramov',
    percent: 21,
    color: 'yellow',
    money: 75000
  }
]

const generateDiagramDateFromAPI = (data) => {
  return data.map(item => ({
    value: item.money,
    name: item.title,
    itemStyle: {
      color: item.color
    }
  }))
}


const SeedRound = (props) => {
  const classes = useStyles()
  const width = useWindowWidth()
  const data = generateDiagramDateFromAPI(items)
  return (
    <>
      <div className={classes.wrapper}>
        <div
          className={classes.backgroundEllipse}
        />
        <Container className={classes.investsContainer}>
          <Grid container alignItems={'center'}>
            <Grid item xs={12} sm={12} md={12} lg={6} container alignItems={'center'}>
              <Grid item xs={12} lg={6}>
                <Typography className={classes.title} variant={'h1'} align={'left'}>
                  Seed round
                </Typography>
                <Typography align={'left'} className={classes.subtitle}>
                  17 February - 15 March
                </Typography>
              </Grid>
              <Grid item xs={12} lg={6} container justify={'flex-end'} className={classes.presentationBtn}>
                <OpenPresentationBtn />
              </Grid>
              <Grid item xs={12}>
                {width > 1280 ? <Points items={items}/> : <Diagram data={data} />}
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={6}>
              {width > 1280 ? <Diagram data={data}/> : <Points items={items}/>}
            </Grid>
          </Grid>
        </Container>
      </div>
    </>
  )
}

export default SeedRound
