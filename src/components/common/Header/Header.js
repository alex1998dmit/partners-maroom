import React from "react"
import {AppBar, Container, makeStyles, SvgIcon, Toolbar, Grid} from "@material-ui/core";
import MaroomIcon from "../../icons/Maroom";
import OpenPresentationBtn from "../../ui/OpenPresentationBtn/OpenPresentationBtn";

const useStyles = makeStyles(theme =>({
  wrapper: {
    flexGrow: 1,
    backgroundColor: '#F2F2F2',
    // backgroundColor: 'transparent',
    position: 'relative',
    // width: '100%',
    // zIndex: 3000
  },
  root: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    border: 'none',
    color: 'black',
  },
  toolbar: {
    display: 'flex',
    width: '100%',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  }
}))

const Header = () => {
  const classes = useStyles()
  return (
    <div className={classes.wrapper}>
      <AppBar
        classes={{
          root: classes.root,
        }}
        position={'static'}
      >
        <Container>
          <Grid container>
            <Toolbar classes={{ root: classes.toolbar }} disableGutters>
              <SvgIcon component={MaroomIcon} />
            </Toolbar>
          </Grid>
        </Container>
      </AppBar>
    </div>
  )
}

export default Header
