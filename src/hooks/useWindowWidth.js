import {useEffect, useState} from "react";
import _ from 'lodash';

const useWindowWidth = () => {
  const [width, setWidth] = useState(0);

  useEffect(() => {
    setWidth(window.innerWidth);
    const handleResize = _.debounce(() => {
      setWidth(window.innerWidth);
    }, 10);

    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return width;
};

export default useWindowWidth;